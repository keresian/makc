from pyramid.renderers import get_renderer
from pyramid.view import view_config
from pyramid.view import view_defaults
from sqlalchemy import func
from cryptacular.bcrypt import BCRYPTPasswordManager
from sqlalchemy.exc import DBAPIError
from sqlalchemy import update
from makc.data_files.skills import SkillList
from makc.data_files.weapons import weapons
from makc.data_files.classes import classes
from makc.data_files.races import races
import base64
import logging
import re

log = logging.getLogger(__name__)


from makc.models import (
    DBSession,
    Users
    )


class CharacterCreate(object):

  
  def __init__(self,request):

    
    self.request = request
    # Add code here to actually check if logged in or not. For now, just a hackjob
    #self.logged_in = authenticated_userid(request)
    self.UserData = request.user
       

    
   
  @view_config(renderer='CharCreate.mako', route_name='charactercreate_first', permission='logged_in')
  def  CharacterCreate_Overview(self):
     javascript = """<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
      <script src="/static/socket.io.js" type="text/javascript"></script>
      <script src="/static/handlebars.js" type="text/javascript"></script>
      <script src="/static/underscore.js" type="text/javascript"></script>
      <script src="/static/backbone.js" type="text/javascript"></script>
      <script src="/dyn/charactercreate.js" type="text/javascript"></script>
      <link href="/static/jquery-ui-1.10.4.custom.css" type="text/css" rel="stylesheet" />"""
      
     Races = races.Races
     Classes = classes.Classes
     log.debug ("CharacterCreate View")
     return { "javascript": javascript, "races": Races, "classes": Classes }
    
