<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel="shortcut icon" href="${request.static_url('makc:static/favicon.ico')}">
% if javascript:
        ${javascript | n}

% endif
  
	<link href="${request.static_url('makc:static/default.css')}" rel="stylesheet" type="text/css">
</head>

<body>
% if request.user is not None:
  <%include file="menu_loggedIn.mako" />
% else:
  <%include file="menu_loggedOut.mako" />
% endif

% if page_title:
  <div class="div-table-caption"><span class="fancy_button_text">${page_title}</span></div>
% endif
<div id="general" class="general" style="margin-top: 250px;">

${next.body()}

</div>

</body>
</html>
