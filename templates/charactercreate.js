$(document).ready(function() {
    WEB_SOCKET_SWF_LOCATION = "/static/WebSocketMain.swf";
    WEB_SOCKET_DEBUG = true;
    var retryCount = 0;
    var select_options="<option value=\"0\">Strength</option><option value=\"1\">Dexterity</option><option value=\"2\">Constitution</option><option value=\"3\">Intelligence</option><option value=\"4\">Wisdom</option><option value=\"5\">Charisma</option>";
    var booted=false;
    var identifier="${identifier}";
    var nickname="${request.user.Username}";
    var room="test";
    var ability0=0;
    var ability1=0;
    var ability2=0;
    var ability3=0;
    var ability4=0;
    var ability5=0;
    var race_id=-1;
    var class_id=-1;
    
    
    // connect to the websocket
    var socket = io.connect('/CharCreate', {
      reconnect: false
     });

    $(window).bind("beforeunload", function() {
        socket.disconnect();
    });

    // Listen for the event "chat" and add the content to the log
    socket.on("chat", function(e) {
        $("#chatlog").append(e + "<br />");

    });
    socket.on("roomchat", function(nick,roomname, e) {
        
        $("#chatlog").append("[" + roomname + "]&lt;" + nick + "&gt;" + e + "<br />");
        $("#chatlog").scrollTop($("#chatlog").get(0).scrollHeight);
        
        
    });
    socket.on("generatedroll", function(something) {
        var myArr = $.parseJSON(something);
        switch(myArr["variety"])
        {
          case 0:
          case 1:
          case 2:
            $("#ability_roll_1").html("<select id=\"ability0\" class=\"ability_select\">" + select_options + "</select> &nbsp; &nbsp;" + myArr["dicetotal1"] + "<br \>");
            $("#ability_roll_2").html("<select id=\"ability1\" class=\"ability_select\">" + select_options + "</select> &nbsp; &nbsp;" + myArr["dicetotal2"] + "<br \>");
            $("#ability_roll_3").html("<select id=\"ability2\" class=\"ability_select\">" + select_options + "</select> &nbsp; &nbsp;" + myArr["dicetotal3"] + "<br \>");
            $("#ability_roll_4").html("<select id=\"ability3\" class=\"ability_select\">" + select_options + "</select> &nbsp; &nbsp;" + myArr["dicetotal4"] + "<br \>");
            $("#ability_roll_5").html("<select id=\"ability4\" class=\"ability_select\">" + select_options + "</select> &nbsp; &nbsp;" + myArr["dicetotal5"] + "<br \>");
            $("#ability_roll_6").html("<select id=\"ability5\" class=\"ability_select\">" + select_options + "</select> &nbsp; &nbsp;" + myArr["dicetotal6"] + "<br \>");
            $("#ability_explanation").html("Please assign the Ability Scores below to the skills of your choosing then continue to choose your Race.<br \> <br \>");
            $("#ability_roll_confirm").html("<button id=\"confirm_abilities\">Confirm</button>");
            $("#confirm_abilities").button();
            $("#confirm_abilities").click(abilityScoreConfirm);
            
            
            

          break;
        }
    });
    socket.on("sendarchtypelist", function(something) {
      var myArr = $.parseJSON(something);
      //console.log(myArr);
      
      for (subobject in myArr) {
        console.log(myArr[subobject]["name"]);
        $("#archtype_list").append("<a href=\"#\" class=\"archtype_select\" id=\"" + myArr[subobject]["index"] + "\">" + myArr[subobject]["name"] + "</a><br \>"); 
        
      }
      $( '.archtype_select' )
          .click(function() {
            alert($(this).attr("id"));
            archtype_id = $(this).attr("id");
            $( "#tabs").tabs("option","disabled", [0,1,2,3] );
            $( "#tabs").tabs("option","active", [4] );
            return false;
           
        });
    });
    
    socket.on("ForceDisconnect", function() {
        booted = true;
        socket.disconnect();
        
        alert("Please refresh the page, you have timed out");
        
    });
    socket.on("error",function(message) {
     if (retryCount < 10) { 
      if (message.indexOf('Bad Gateway') >= 0) {
        
        setTimeout(function() {
          socket.socket.connect();
          retryCount++;
          
        },1000);
      }
     }
    });
    socket.on("disconnect", function() {
      if (!booted) {
        //alert("disconnected");
        setTimeout(function() {
          socket.socket.connect();
         
          
        },1000);
      }   
    }); 
      
      
      
    socket.on("already_rolled",function(jsontext) {
      var myArr = $.parseJSON(jsontext);
      
    });           
    socket.on("user_disconnect", function() {
        $("#chatlog").append("user disconnected" + "<br />");
    });

    socket.on("connect", function() {
       // $("#chatlog").append("user connected" + "<br />");
       // socket.emit('join', nickname, 'test');
        retryCount = 0;
        
    });

    // Execute whenever the form is submitted
    $("#chat_form").submit(function(e) {
        // don't allow the form to submit
        e.preventDefault();
        
        var val = $("#chatbox").val();

        // send out the "chat" event with the textbox as the only argument
        socket.emit("chat", val, room);

        $("#chatbox").val("");
    });
    
    
 // BEGIN NON SOCKET-IO CODE //    
    function abilityScoreConfirm(e) {
      // We need to validate our abilities, make sure they are all set right before opening the next tab.
      var abilities = new Array();
      var duplicates = 0;
      abilities[$("#ability0").val()] = 1;
      abilities[$("#ability1").val()] = 1;
      abilities[$("#ability2").val()] = 1;
      abilities[$("#ability3").val()] = 1;
      abilities[$("#ability4").val()] = 1;
      abilities[$("#ability5").val()] = 1;
      if (abilities.length == 6) {
        for(var i=0;i<abilities.length;i++)
        {
          if (abilities[i] != 1) {
            duplicates = 1;
            break;
          } 
        }
        if (duplicates) {
            alert("You must assign a value to each ability");
        }
        else {
            $( "#tabs").tabs("option","disabled", [0,2,3,4] );
            $( "#tabs").tabs("option","active", [1] );
        }
      } else {
            alert("You must assign a value to each ability");
      }
    }
        
        //$( "#tabs" ).tabs();
        $( "#standard_roll" ).tooltip();
        $( '.ability_roll' )
          .click(function() {
            switch ($(this).html())
            {
              case "Standard":
                socket.emit('rollabilitydice',0,identifier);
                break;
              case "Classic":
                socket.emit('rollabilitydice',1,identifier);
                break;
              case "Heroic":
                socket.emit('rollabilitydice',2,identifier);
                break;
              case "Purchase":
                //socket.emit('rollabilitydice',3,identifier);
                alert("Purchase");
                break;

            }
            return false;           
        });
        // Custom tooltips for tooltips ( RACE THIS TIME )!
        
        $( ".race_select" ).tooltip({
          
            tooltipClass: "race_tooltip_styling",
            content: function() {
              var text = $( this ).attr("title");
              return text; 
            } 
        });
        
        $( '.race_select' )
          .click(function() {
            alert($(this).attr("id"));
            race_id = $(this).attr("id");
            $( "#tabs").tabs("option","disabled", [0,1,3,4] );
            $( "#tabs").tabs("option","active", [2] );
            return false;
           
        });

        $( ".class_select" ).tooltip({
          
            tooltipClass: "race_tooltip_styling",
            content: function() {
              var text = $( this ).attr("title");
              return text; 
            } 
        });
        
        $( '.class_select' )
          .click(function() {
            alert($(this).attr("id"));
            class_id = $(this).attr("id");
            socket.emit('getarchtypelist',race_id,class_id,identifier);
            $( "#tabs").tabs("option","disabled", [0,1,2,4] );
            $( "#tabs").tabs("option","active", [3] );
            return false;
           
        });
        
        $( "#classic_roll" ).tooltip();
    

        
        $( "#heroic_roll" ).tooltip();
       
        
        $( "#point_purchase" ).tooltip();

                 
 
  $( "#tabs").tabs();
  $( "#tabs").tabs("option","disabled", [1,2,3,4] );

 // $( "#CharCreationList").dialog({ minWidth:330});
 // $( "#CharCreationList").dialog({ maxWidth:330});
 // $( "#CharCreationList").dialog({ height:330});
 // $( "#CharCreationList").dialog({ title:"Steps to Complete"});
 // $( "#CharCreationList").dialog({ draggable: true});
 // $( "#CharCreationList").dialog({ resizable:true});
 // $( "#CharCreationList").dialog({ modal:false});        
 // $( "#CharCreationList").dialog("option", "position",  { my: "right top", at: "right-10 top+10", of: $("#general") } )

});
