<%inherit file="global_layout.mako" />
<div class="character-list"><center>Characters</center>
% if charlist:
%   for x, v in charlist:
      ${x} ${v}
% endfor

% else:
      No Characters<br />
% endif
<br /><br /><a href="${request.route_url('charactercreate_first')}">Create Character</a>
</div>