from classes import classes

class SkillList(object):
    skill = [
      { "Skill": "Acrobatics",                "ClassFlag": 17542 , "Untrained": 1, "ACP": 1, "KA": "Dex" },
      { "Skill": "Appraise",                  "ClassFlag": 311317, "Untrained": 1, "ACP": 0, "KA": "Int" },
      { "Skill": "Bluff",                     "ClassFlag": 49548 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Climb",                     "ClassFlag": 26606 , "Untrained": 1, "ACP": 1, "KA": "Str" },
      { "Skill": "Craft Alchemy",             "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Armor",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Baskets",             "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Books",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Bows",                "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Calligraphy",         "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Carpentry",           "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Cloth",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Clothing",            "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Glass",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Jewelry",             "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Leather",             "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Locks",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Paintings",           "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Pottery",             "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Sculptures",          "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Ships",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Shoes",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Stonemasonry",        "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Traps",               "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Craft Weapons",             "ClassFlag": 524287, "Untrained": 1, "ACP": 0, "KA": "Int" }, 
      { "Skill": "Diplomacy",                 "ClassFlag": 22812 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Disable Device",            "ClassFlag": 16385 , "Untrained": 0, "ACP": 1, "KA": "Dex" },
      { "Skill": "Disguise",                  "ClassFlag": 16644 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Escape Artist",             "ClassFlag": 17412 , "Untrained": 1, "ACP": 1, "KA": "Dex" },
      { "Skill": "Fly",                       "ClassFlag": 492065, "Untrained": 1, "ACP": 1, "KA": "Dex" },
      { "Skill": "Handle Animal",             "ClassFlag": 78058 , "Untrained": 0, "ACP": 0, "KA": "Cha" },
      { "Skill": "Heal",                      "ClassFlag": 145841, "Untrained": 1, "ACP": 0, "KA": "Wis" },
      { "Skill": "Intimidate",                "ClassFlag": 190414, "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Knowledge (Arcana)",        "ClassFlag": 492309, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Dungeoneering)", "ClassFlag": 353092, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Engineering)",   "ClassFlag": 327876, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Geography)",     "ClassFlag": 335908, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (History)",       "ClassFlag": 461844, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Local)",         "ClassFlag": 344196, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Nature)",        "ClassFlag": 467239, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Nobility)",      "ClassFlag": 331796, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Planes)",        "ClassFlag": 461588, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Knowledge (Religion)",      "ClassFlag": 335124, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Linguistics",               "ClassFlag": 344084, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Perception",                "ClassFlag": 26023 , "Untrained": 1, "ACP": 0, "KA": "Wis" },
      { "Skill": "Perform: Act",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: Comedy",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: Dance",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: Keyboard Instruments",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: Oratory",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: Percussion Instruments",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: String Instruments",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: Wind Instruments",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Perform: Sing",                   "ClassFlag": 17412 , "Untrained": 1, "ACP": 0, "KA": "Cha" },
      { "Skill": "Profession: Architect",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Baker",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Barrister",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Brewer",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Butcher",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Clerk",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Cook",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Courtesan",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Driver",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Engineer",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Farmer",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Fisherman",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Gambler",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Gardener",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Herbalist",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Innkeeper",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Librarian",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Merchant",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Midwife",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Miller",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Miner",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Porter",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Sailor",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Scribe",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Shepherd",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Stable Master",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Soldier",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Tanner",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Trapper",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" },
      { "Skill": "Profession: Woodcutter",                "ClassFlag": 524285, "Untrained": 0, "ACP": 0, "KA": "Wis" }, 
      { "Skill": "Ride",                      "ClassFlag": 79850 , "Untrained": 1, "ACP": 1, "KA": "Dex" },
      { "Skill": "Sense Motive",              "ClassFlag": 23836 , "Untrained": 1, "ACP": 0, "KA": "Wis" },
      { "Skill": "Sleight of Hand",           "ClassFlag": 16517 , "Untrained": 0, "ACP": 1, "KA": "Dex" },
      { "Skill": "Spellcraft",                "ClassFlag": 506677, "Untrained": 0, "ACP": 0, "KA": "Int" },
      { "Skill": "Stealth",                   "ClassFlag": 25860 , "Untrained": 1, "ACP": 1, "KA": "Dex" },
      { "Skill": "Survival",                  "ClassFlag": 8675  , "Untrained": 1, "ACP": 0, "KA": "Wis" },
      { "Skill": "Swim",                      "ClassFlag": 26602 , "Untrained": 1, "ACP": 1, "KA": "Str" },
      { "Skill": "Use Magic Device",          "ClassFlag": 246277, "Untrained": 0, "ACP": 0, "KA": "Cha" }
                                                              
    ]
    
    @staticmethod
    def debug_ID_to_ClassList(SkillSearch):
        classList = ''
        classBitField = SkillList.skill[SkillSearch]["ClassFlag"]
        for y in classes.Classes:
          
          if y["bitflag"] & classBitField:
            classList = classList + " " + y["Class"]
          
            
        
        return classList.strip()   
    ##Weapons.append({ 'Name': "sword", 'Damage': "2d20" })

      