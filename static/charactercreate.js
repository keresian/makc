$(document).ready(function() {
    WEB_SOCKET_SWF_LOCATION = "/static/WebSocketMain.swf";
    WEB_SOCKET_DEBUG = true;
    var retryCount = 0;
    var booted=false;
    var nickname=prompt("Please enter your name","");
    var room="test";
    // connect to the websocket
    var socket = io.connect('/CharCreate', {
      reconnect: false
     });

    $(window).bind("beforeunload", function() {
        socket.disconnect();
    });

    // Listen for the event "chat" and add the content to the log
    socket.on("chat", function(e) {
        $("#chatlog").append(e + "<br />");

    });
    socket.on("roomchat", function(nick,roomname, e) {
        
        $("#chatlog").append("[" + roomname + "]&lt;" + nick + "&gt;" + e + "<br />");
        
        
    });
    socket.on("ForceDisconnect", function() {
        booted = true;
        socket.disconnect();
        
        alert("You connected from another instance, disconnecting");
        
    });
    socket.on("error",function(message) {
     if (retryCount < 10) { 
      if (message.indexOf('Bad Gateway') >= 0) {
        
        setTimeout(function() {
          socket.socket.connect();
          retryCount++;
          
        },1000);
      }
     }
    });
    socket.on("disconnect", function() {
      if (!booted) {
        alert("disconnected");
        setTimeout(function() {
          socket.socket.connect();
         
          
        },1000);
      }   
    }); 
      
      
      
      
    socket.on("user_disconnect", function() {
        $("#chatlog").append("user disconnected" + "<br />");
    });

    socket.on("connect", function() {
       // $("#chatlog").append("user connected" + "<br />");
        socket.emit('join', nickname, 'test');
        retryCount = 0;
    });

    // Execute whenever the form is submitted
    $("#chat_form").submit(function(e) {
        // don't allow the form to submit
        e.preventDefault();

        var val = $("#chatbox").val();

        // send out the "chat" event with the textbox as the only argument
        socket.emit("chat", val, room);

        $("#chatbox").val("");
    });

});
