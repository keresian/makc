from pyramid.renderers import get_renderer
from pyramid.view import view_config
from pyramid.view import view_defaults
from sqlalchemy import func
from cryptacular.bcrypt import BCRYPTPasswordManager
from sqlalchemy.exc import DBAPIError
from sqlalchemy import update
from makc.data_files.skills import SkillList
from makc.data_files.weapons import weapons
from makc.data_files.classes import classes

import base64
import logging
import re
import redis

log = logging.getLogger(__name__)


from makc.models import (
    DBSession,
    Users
    )


class ProfileOptions(object):

  
  def __init__(self,request):

    
    self.request = request
    # Add code here to actually check if logged in or not. For now, just a hackjob
    #self.logged_in = authenticated_userid(request)
    self.UserData = request.user
       

    
   
  @view_config(renderer='profile_default.mako', route_name='profile_default', permission='logged_in')
  def  default_profile(self):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    log.debug(r.get('foo'))
    log.debug ("For Skill: " + SkillList.skill[0]["Skill"] + ", the Skilled Classes are: " + SkillList.debug_ID_to_ClassList(0))
    ##log.debug (SkillList.skill[0]["Skill"])
    log.debug ("Main Profile View")
    return { "HabitatCount" : "20", "ShipCount" : "10" }
    
