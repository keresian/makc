import logging
log = logging.getLogger(__name__)

class classes(object):
    Classes = [
    { "index": 0, "Class": "Alchemist", "bitflag": 1, "Description": "Some Description" },
    { "index": 1, "Class": "Barbarian", "bitflag": 2, "Description": "Some Description" },
    { "index": 2, "Class": "Bard", "bitflag": 4, "Description": "Some Description" },
    { "index": 3, "Class": "Cavalier", "bitflag": 8, "Description": "Some Description" },
    { "index": 4, "Class": "Cleric", "bitflag": 16, "Description": "Some Description" },
    { "index": 5, "Class": "Druid", "bitflag": 32, "Description": "Some Description" },
    { "index": 6, "Class": "Fighter", "bitflag": 64, "Description": "Some Description" },
    { "index": 7, "Class": "Gunslinger", "bitflag": 128, "Description": "Some Description" },
    { "index": 8, "Class": "Inquisitor", "bitflag": 256, "Description": "Some Description" },
    { "index": 9, "Class": "Magus", "bitflag": 512, "Description": "Some Description" },
    { "index": 10, "Class": "Monk", "bitflag": 1024, "Description": "Some Description" },
    { "index": 11, "Class": "Oracle", "bitflag": 2048, "Description": "Some Description" },
    { "index": 12, "Class": "Paladin", "bitflag": 4096, "Description": "Some Description" },
    { "index": 13, "Class": "Ranger", "bitflag": 8192, "Description": "Some Description" },
    { "index": 14, "Class": "Rogue", "bitflag": 16384, "Description": "Some Description" },
    { "index": 15, "Class": "Sorcerer", "bitflag": 32768, "Description": "Some Description" },
    { "index": 16, "Class": "Summoner", "bitflag": 65536, "Description": "Some Description" },
    { "index": 17, "Class": "Witch", "bitflag": 131072, "Description": "Some Description" },
    { "index": 18, "Class": "Wizard", "bitflag": 262144, "Description": "Some Description" }
    ]
    
    @staticmethod
    def debug_ID_to_Name(ClassSearch):
        classList = ''
        for k , y in Classes.iteritems():
          
          if y & ClassSearch:
            classList = classList + " " + k
          
            
        
        return classList.strip()   