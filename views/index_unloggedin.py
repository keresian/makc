from pyramid.renderers import get_renderer
from pyramid.view import view_config
from pyramid.view import view_defaults
from pyramid.view import forbidden_view_config
from pyramid.response import Response
from pyramid.security import remember, forget, authenticated_userid
from sqlalchemy import func
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message
from pyramid.httpexceptions import HTTPFound
from cryptacular.bcrypt import BCRYPTPasswordManager
from sqlalchemy.exc import DBAPIError
from sqlalchemy import update
import base64
import logging
import re

log = logging.getLogger(__name__)


from makc.models import (
    DBSession,
    Users,
    )


class NotLoggedInView(object):
 
  def __init__(self,request):
    self.request = request
    # Add code here to actually check if logged in or not. For now, just a hackjob
    self.UserData = request.user



  @view_config(renderer='home.mako', route_name='home')
  def get(self):
    log.debug("test")
    return {"page_title": "test"}

  @view_config(renderer='about.mako', route_name='about')
  def about(self):
      return {"page_title": "About" }
      
  @view_config(renderer='signup.mako', route_name='signup', request_method="GET")
  def signup(self):
      return {"page_title": "Sign Up!" , "errorMessage" : None , "username" : "",
         "password" : "", "confirmpassword" : "" , "email" : "" }
  
 # Process the sign up form #
 # To Do: 
 # send email validations
 
  @view_config(renderer='signup.mako', route_name='signup', request_method="POST")
  def signup_process(self):
      username = self.request.params['Username'].strip()
      password = self.request.params['password'].strip()
      confirmpassword = self.request.params['confirmpassword'].strip()
      email = self.request.params['email'].strip()
      if len(username) < 3 or len(username) > 32 or len(password) < 8 or len(password) > 50:
          errorMessage = "ERROR: Username or Password too short!"
          log.debug("Username or password too Short")
          log.debug(errorMessage)
          return { "errorMessage" : errorMessage , "username" : username, "password" : password, "confirmpassword" : confirmpassword, 
            "email" : email}
      
      user = Users()
      user.Username = self.request.params['Username'].strip()
  
      ## Verify passwords match
      if self.request.params['password'].strip() != self.request.params['confirmpassword'].strip():
          errorMessage = "ERROR: Passwords do not match"
          log.debug("Passwords don't match")
          log.debug(errorMessage)
          return { "errorMessage" : errorMessage , "username" : username, "password" : password, "confirmpassword" : confirmpassword, 
            "email" : email}
              
       ## Encrypt password       
      manager = BCRYPTPasswordManager()
      hashed_password = manager.encode(self.request.params['password'].strip())
      if manager.check(hashed_password, self.request.params['password'].strip()):
          user.Password = hashed_password
      else:
          return { "page_title" : "Sign Up", "errorMessage" : "Unknown Error occured with password" , "username" : username, "password" : password, "confirmpassword" : confirmpassword, 
            "email" : email}
      
      ## verify Email Address is formatted correctly
      if len(self.request.params['email'].strip()) > 7:
        if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", self.request.params['email'].strip()) is None:
          return { "page_title" : "Sign up" , "errorMessage" : "email address invalid" , "username" : username, "password" : password, "confirmpassword" : confirmpassword, 
            "email" : email}
      else:
        return { "page_title": "Sign up" , "errorMessage" : "email address invalid" , "username" : username, "password" : password, "confirmpassword" : confirmpassword, 
            "email" : email}
            
      user.EmailAddress = self.request.params['email'].strip()
      ## Check if Email Address used before
      q = DBSession.query(Users.EmailAddress).filter(Users.EmailAddress.ilike(self.request.params['email']))
      if q.count() > 0:
        return { "page_title": "Sign Up" , "errorMessage" : "email address already in use" , "username" : username, "password" : password, "confirmpassword" : confirmpassword, 
            "email" : email}
   

      ## Check if username already in use
      q = DBSession.query(Users.Username).filter(Users.Username.ilike(self.request.params['Username']))
      if q.count() > 0:
        return { "page_title": "Sign Up" , "errorMessage" : "Username already exists" , "username" : username, "password" : password, "confirmpassword" : confirmpassword, 
            "email" : email}
      ## Create verification Code and send email, as well as add to db
      verification_key = base64.urlsafe_b64encode(manager.encode(self.request.params['Username']+"12Password45"))
      ## base64.urlsafe_b64encode
      user.VerifiedKey = verification_key
      DBSession.add(user)
      mailer = get_mailer(self.request)
      message = Message(subject="Test Email!",
                  sender="admin@meadowsandkittycats.com",
                  recipients=[self.request.params['email'].strip()],
                  body="Welcome to Meadows and Kitty Cats!   Please follow the link to complete your registration! \n\n" + self.request.route_url('validate',validation_code=verification_key))
      
      mailer.send(message)          
      #log.debug (dir(user))
      return {"page_title": "Complete" , "errorMessage" : "Complete"}        
        
        
  @view_config(renderer='validate.mako', route_name='validate')
  def validate(self):
    matchdict = self.request.matchdict
    validatercode = matchdict.get('validation_code', None)
    q = DBSession.query(Users).filter(Users.VerifiedKey == validatercode , Users.Verified == 0)
    if q.first() > 0:
      #log.debug(dir(q))
      q.update({"Verified": 1})
 
      
      return { "message" : "You have successfully validated your account.  Please Log In using the button above."}
    else:
      return { "message" : "invalid code" }
  
  @view_config(renderer='login.mako', route_name='login')
  @forbidden_view_config(renderer='login.mako')
  def login(self):
      return { "errorMessage" : None}

  
  @view_config(renderer='login.mako', route_name='login', request_method="POST")
  def login_process(self):
      request = self.request
      email = request.params['email'].strip()
      password = request.params['password'].strip()
      manager = BCRYPTPasswordManager()
      hashed_password = manager.encode(password)
      q = DBSession.query(Users).filter(Users.EmailAddress.ilike(email))
      User = q.first()
      if User is not None:
        log.debug(dir(User))
        if manager.check(User.Password, request.params['password'].strip()):
          headers = remember(request, User.UserID)
          return HTTPFound(location=request.route_url('profile_default'), headers=headers)
        else:
          return { "errorMessage" : "Username or Password invalid" }
      else:
        return { "errorMessage" : "Username or Password invalid, or account not verified" }
				
	
#  def post(self):
#    return Response('post')
		
#  def delete(self):
#    return Response('delete')
	  