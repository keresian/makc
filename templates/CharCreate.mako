  <%inherit file="global_layout.mako" />
  <div id="CharCreateContainer">
    <h1>Character Creation</h1>
    Please complete the sections below to begin creation of your character.<br \>
    <br \>
    <br \>
    <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Abilities</a></li>
    <li><a href="#tabs-2">Race</a></li>
    <li><a href="#tabs-3">Class</a></li>
    <li><a href="#tabs-4">Archtype</a></li>
    <li><a href="#tabs-5">Confirm Selections</a></li>
  </ul>
    
  <div id="tabs-1">
  <p>
    <h2>Abilities</h2>
    <span id="ability_explanation"></span>
    <span id="ability_roll_1">Please choose the method you would like to use to generate your abilities.  Keep in mind some DM's may not allow you to join their game if the method you choose is going to unbalance their campaign.  The most common choices are "Classic" and "Point Buy"</span>
    <span id="ability_roll_2"><br \><br \>Mouse over each option to receive an explanation of how your ability scores will be generated.<br \>Once you click on an option, you can not change your mind nor change your dice rolls for 24 hours<br \><br \></span>
    <span id="ability_roll_3"><a href="#" class="ability_roll" id="standard_roll" title="This method of generation rolls 4d6, and discards the lowest, then adds the remaining ones together for 1 ability score">Standard</a><br \></span>
    <span id="ability_roll_4"><a href="#" class="ability_roll" id="classic_roll" title="This method of generation rolls 3d6, and adds them together for 1 ability score">Classic</a><br \></span>
    <span id="ability_roll_5"><a href="#" class="ability_roll" id="heroic_roll" title="This method of generation rolls 2d6, adds them together, then adds 6 for 1 ability score">Heroic</a><br \></span>
    <span id="ability_roll_6"><a href="#" class="ability_roll" id="point_purchase" title="This method of generation uses a point buy system, where you have 15 points to distribute, each additional point in a skill costing slightly more">Purchase</a><br \></span>
    <span id="ability_roll_confirm"></span>
  </div>
  <div id="tabs-2">
    Choose a Race below:<br \>
    % for val in races:
    <span class="race_class" id="race_list${val["index"]}"><a href="#" class="race_select" id="${val["index"]}" title="${val["Description"] | n}">${val["Race"] | n}</a><br \></span>
    % endfor
    
    
  </div>
  <div id="tabs-3">
    Choose a Class below:<br \>
    % for val in classes:
    <span class="class_class" id="class_list${val["index"]}"><a href="#" class="class_select" id="${val["index"]}" title="${val["Description"] | n}">${val["Class"] | n}</a><br \></span>
    % endfor
  </div>
  <div id="tabs-4">
    Choose your archtype:<br \>
    <span id="archtype_list"></span>
  </div>
  <div id="tabs-5">
    Confirm your selections below: <br \>
    <ul>
      <li>Class: <span id="class_selected"></span></li>
      <li>Race: <span id="race_selected"></span></li>
      
    
  </div>
 </div>
  
  
  
  
<!--
  <div id="CharCreationList">
    <ul>
      <li> Abilities
      <li> Race
      <li> Class
    </ul>
  </div>
  ---!>
    <h1>Chat Log</h1>
    <div id="chatlog" style="height: 200px; width: 500px; position: relative; overflow:auto"></div><br \>
    <form id="chat_form">

      <input type="text" id="chatbox"></input>
      <button type="submit" id="submit">Send</button>
    </form>

  </div>


