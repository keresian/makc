<%inherit file="global_layout.mako" />
% if errorMessage != "Complete":
	<form method="post" action="${request.route_url('signup')}">
		<br />
		% if errorMessage is not None:
			<%block> <font color='red'>${errorMessage}</font> </%block>
		% endif
		<b></b>
		<fieldset>
			<legend>Required Information:</legend>
			<table>
			
				
			<tr><td>Ingame Name:</td><td> <input type="text" size="30" name="Username" value="${username}"/>*3 to 32 characters</td></tr>
			
			<tr><td>Password:</td><td> <input type="password" size="30" name="password" value="${password}"> *8 to 50 characters <br /></td></tr>
			<tr><td>Confirm Password:</td><td> <input type="password" size="30" name="confirmpassword" value="${confirmpassword}"/><br /></td></tr>
			<tr><td>E-mail:</td><td> <input type="text" size="30" name="email" value="${email}"/> *must be valid</td></tr>
			<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
			<tr><td></td><td><input type="submit" value="Submit" /></td></tr>
		</table>
		</fieldset>
	</form>
% else:
  Registration Complete - Please check your email to confirm your information.
% endif