from pyramid.renderers import get_renderer
from pyramid.view import view_config
from pyramid.view import view_defaults
from sqlalchemy import func

from sqlalchemy.exc import DBAPIError
from sqlalchemy import update
import hashlib
import redis
import base64
import logging
import re

log = logging.getLogger(__name__)


from makc.models import (
    DBSession,
    Users
    )


class DynamicJavascript(object):

  
  def __init__(self,request):

    
    self.request = request
    # Add code here to actually check if logged in or not. For now, just a hackjob
    #self.logged_in = authenticated_userid(request)
    self.UserData = request.user
       

    
   
  @view_config(renderer='charactercreate.js', route_name='dyn_charcreate', permission='logged_in')
  def  dynamic_charactercreate(self):
     identifier = hashlib.sha256("123"+self.request.user.Username+"456").hexdigest()
     r = redis.StrictRedis(host='localhost', port=6379, db=0)
     r.set("allowed:"+identifier,1)
     r.expire("allowed:"+identifier,3600)
    
    
    
     return { "identifier" : identifier }
    # Need to add code to return a unique identifier, which we store so that socket.io project can find it and associate changes with this user
     
