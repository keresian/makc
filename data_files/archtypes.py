class archtypes(object):
  archtype = [
    ## Alchemist
    { "class":0, "race":-1, "name": "Normal Archtype" },
    { "class":0, "race":-1, "name": "Beastmorph" },
    { "class":0, "race":-1, "name": "Chirurgeon" },
    { "class":0, "race":-1, "name": "Clone Master" },
    { "class":0, "race":-1, "name": "Crypt Breaker" },
    { "class":0, "race":-1, "name": "Grenadier" },    
    { "class":0, "race":-1, "name": "Internal Alchemist" },
    { "class":0, "race":-1, "name": "Mindchemist" },
    { "class":0, "race":-1, "name": "Visionary Researcher" },
    { "class":0, "race":-1, "name": "Preservationist" },
    { "class":0, "race":-1, "name": "Psychonaut" },
    { "class":0, "race":-1, "name": "Ragechemist" },
    { "class":0, "race":-1, "name": "Reanimator" },
    { "class":0, "race":-1, "name": "Vivisectionist" },
    { "class":0, "race":3, "name": "Bramble Brewer" },
    { "class":0, "race":2, "name": "Saboteur" },
    ## Barbarian
    { "class":1, "race":-1, "name": "Normal Archtype" },
    { "class":1, "race":-1, "name": "Breaker" },
    { "class":1, "race":-1, "name": "Brutal Pugilist" },
    { "class":1, "race":-1, "name": "Drunken Brute" },
    { "class":1, "race":-1, "name": "Elemental Kin" },
    { "class":1, "race":-1, "name": "Hurler" },
    { "class":1, "race":-1, "name": "Invulnerable Rager" },
    { "class":1, "race":-1, "name": "Mad Dog" },
    { "class":1, "race":-1, "name": "Mounted Fury" },
    { "class":1, "race":-1, "name": "Savage Barbarian" },
    { "class":1, "race":-1, "name": "Scarred Rager" },
    { "class":1, "race":-1, "name": "Sea Reaver" },
    { "class":1, "race":-1, "name": "Superstitious" },
    { "class":1, "race":-1, "name": "Titan Mauler" },
    { "class":1, "race":-1, "name": "Totem Warrior" },  
    { "class":1, "race":-1, "name": "True Primitive" },
    { "class":1, "race":-1, "name": "Urban Barbarian" },
    { "class":1, "race":-1, "name": "Wild Rager" },
    { "class":1, "race":12, "name": "Feral Gnasher" },
    { "class":1, "race":4, "name": "Hateful Rager" }
#### NEED TO ADD RACIAL ARCHTYPES FOR BARBARIAN
  ]
  