from pyramid.view import view_config
import logging
import time
log = logging.getLogger(__name__)



@view_config(renderer='chat.mako', route_name='chat')
def index(request):
    """ Base view to load our template """
    return {}


